package com.agiledeveloper;

import akka.actor.UntypedActor;

public class Explorer extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {
            try {
                LinksFinder.getLinks((String) message)
                           .forEach(url -> sender().tell(url, self()));
            } catch (Exception e) {
                sender().tell(e, self());
            } finally {
                sender().tell(new Completed(), self());
            }
        }
    }
}
