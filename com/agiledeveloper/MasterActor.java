package com.agiledeveloper;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;

import java.util.HashSet;
import java.util.Set;

public class MasterActor extends UntypedActor {
    private ActorRef originalSender;
    private Set<String> visited = new HashSet<>();
    private ActorRef visitors;
    private long taskCount = 0;

    @Override
    public void preStart() throws Exception {
        super.preStart();
        visitors = getContext().actorOf(
                Props.create(Explorer.class).withRouter(new RoundRobinPool(40))
        );
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (originalSender == null) originalSender = sender();
        if (message instanceof String) {
            if (!visited.contains(message)) {
                visited.add((String) message);
                taskCount++;
                visitors.tell(message, self());
            }
        }
        if (message instanceof Completed) {
            taskCount--;
        }
        if (message instanceof Exception) {
            originalSender.tell(message, self());
        }
        if (taskCount == 0) {
            originalSender.tell((long) visited.size(), self());
        }
    }
}
