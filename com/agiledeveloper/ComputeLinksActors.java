package com.agiledeveloper;
import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.pattern.Patterns;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import java.util.concurrent.TimeUnit;

public class ComputeLinksActors implements ComputeLinks {
  public long countLinks(String url) {
    ActorSystem actorSystem = ActorSystem.create();

    ActorRef masterActor = actorSystem.actorOf(Props.create(MasterActor.class));

    final Future<Object> result = Patterns.ask(masterActor, url, 20000);
    try {
        final Object response = Await.result(result, Duration.apply(20000, TimeUnit.MILLISECONDS));
        if(response instanceof Exception)
          throw new RuntimeException((Exception) response);

        return (long) response;
    } catch (Exception ex) {
        throw new RuntimeException(ex);
    } finally {
      actorSystem.shutdown();
    }
  }
}
